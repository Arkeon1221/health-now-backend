const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')


// User registration
module.exports.addUser = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)
	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		address: data.address,
		postcode: data.postcode,
		mobileNumber: data.mobileNumber,
		email: data.email,
		userName: data.userName,
		password: encrypted_password
	})

return User.find({email: data.email}).then((result) => {
			if(result.length > 0){
				return {Message: "Email already exist!"}
			} else {

				return new_user.save().then((created_user, error) => {
					if(error){
						return false 
					}

					return {
						message: 'User successfully added!'
					}
				})

			}

			
		})
}
	

// Login User
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return {
				message: `Login successful! Welcome ${result.firstName}!`,
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}

		
	})
}



// Update User information (Admin only)
module.exports.editUser = (user_id, data) => {

	if(data.isAdmin) {

		return User.findByIdAndUpdate(user_id, {
			firstName: data.user.firstName,
			lastName: data.user.lastName,
			address: data.user.address,
			postcode: data.user.postcode,
			mobileNumber: data.user.mobileNumber,
			email: data.user.email,
			userName: data.user.userName,
			password: data.user.password
		}).then((result, error) => {
			if(error){
				return false
			}

			return {
				message: 'User has been updated successfully!'
			}
		})


	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this!'
	})

	return message.then((value) => {
		return value
	})	
}


// View ALL users (ADMIN onlyy)

module.exports.viewAllUsers = (data) => {

	if(data.isAdmin){
		return User.find({}).then((result) => {
			return result
		})

	}

		let message = Promise.resolve({
		message: 'User must be ADMIN to access this!'
	})

	return message.then((value) => {
		return value
	})
	
}



// Delete USER (ADMIN only)

module.exports.deleteUser = (data, user_Id) => {

	if(data.isAdmin){

		return User.findByIdAndDelete(user_Id).then((admin_set, error) => {
			if(error){
				return false
			}

			return {
				message:  'The user account has been deleted successfully!'
			}
		})

	}	

			let message = Promise.resolve({
			message: 'User must be ADMIN to access this!'
		})

		return message.then((value) => {
			return value
		})
}



// Delete Multiple USER (ADMIN only)

module.exports.deleteMulti = (data) => {

	if(data.isAdmin){

		return User.deleteMany({_id: {$in: data.user}}).then((deleteMulti, error) => {
			if(error){
				return false
			}

			return {
				message:  'User accounts has been deleted successfully'
			}
		})

	}	

			let message = Promise.resolve({
			message: 'User must be ADMIN to access this!'
		})

		return message.then((value) => {
			return value
		})
}

