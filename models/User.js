const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},

	address: {
		type: String,
		required: [true, 'Address is required.']
	},

	postcode: {
		type: String,
		required: [true, 'Postcode is required.']
	},

	mobileNumber: {
		type: String, 
		required: [true, 'Mobile number is required.']
	},

	email: {
		type: String,
		required: [true, 'Email name is required.']
	},

	userName: {
		type: String,
		required: [true, 'User name is required.']
	},

	password: {
		type: String,
		required: [true, 'Password is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	}	

})
module.exports = mongoose.model('User', user_schema)