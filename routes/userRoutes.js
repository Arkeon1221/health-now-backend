const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')


// User registration
router.post("/addUser", (request, response) => {
	UserController.addUser(request.body).then((result) => {
		response.send(result)
	})
})

// Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})



// Update User information (Admin only)

router.patch('/edit/:userId', auth.verify, (request, response) => {

	const data = {
	
			user: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

	
	UserController.editUser(request.params.userId, data).then((result) => {
		response.send(result)
	})
})


// View ALL users (ADMIN onlyy)

router.get("/ViewAllUsers", auth.verify, (request, response) => {

	const data = {

		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.viewAllUsers(data, request.params.id).then((result) => {

		response.send(result)
	})
})


// Delete USER account (ADMIN only)

router.delete('/delete/:userId', auth.verify, (request,response) => {

	const data = {

		user_Id: request.params.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.deleteUser(data, request.params.userId).then((result) => {
		response.send(result)
	})
})


// Delete Multiple users

router.delete("/deleteMulti", auth.verify, (request, response) => {

	
	let data = {
		user: request.body.ids,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.deleteMulti(data, request.body.ids).then((result) => {
		response.send(result)

	})
})

module.exports = router