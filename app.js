const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')

dotenv.config()

const app = express()
const port = 8001

// MongoDB Connection
mongoose.connect(`mongodb+srv://healthnowdummy:${process.env.MONGODB_PASSWORD}@cluster0.zw7o1nc.mongodb.net/Health-Now-Booking-System?retryWrites=true&w=majority`, {
	
	
	useNewUrlParser: true,
	useUnifiedTopology: true 

})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongopDB'))
// MongoDB Connection END

// To avoid CORS errors when trying to send request to our server
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/users', userRoutes)
// Routes END

app.listen(port, () => {
	console.log(`API is now running on localhost: ${port}`)
}) 